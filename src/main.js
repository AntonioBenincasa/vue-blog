import Vue from 'vue'
import App from './App.vue'

import { BootstrapVue } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import router from './router'
import store from './store'
import Vuelidate from 'vuelidate';

Vue.config.productionTip = false

Vue.use(BootstrapVue)
Vue.use(Vuelidate)

store.subscribe((mutate, state) => {
  localStorage.setItem('state', JSON.stringify(state));
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
