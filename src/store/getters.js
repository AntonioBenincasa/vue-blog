import {map} from 'lodash';
export default {
    getPostById: (state) => (id) => {
        id = Number(id);
        return state.posts.find(post => post.id === id);
    },
    getUsersForSelect: ({users}) => {
        let values = [];
        values = map(users, (user) => {
            return {
                text: `${user.name} | ${user.username} | ${user.email}`,
                value: user.id
            }
        })

        values.unshift({
            text: 'Select an author',
            value: null
        })
        return values;
    }
}