export default {
  getPosts: ({ commit }) => {
    const state = JSON.parse(localStorage.getItem("state"));
    if (state && state.posts && state.posts.length > 0) {
      commit("setPosts", state.posts);
    } else {
      fetch("https://jsonplaceholder.typicode.com/posts")
        .then((res) => res.json())
        .then((res) => {
          commit("setPosts", res)
        });
    } 
  },
  addPost: ({ commit }, values) => {
    fetch("https://jsonplaceholder.typicode.com/posts", {
      method: "POST",
      body: JSON.stringify(values),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        commit("addPost", json);
      });
  },
  editPost: ({ commit }, post) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${post.id}`, {
      method: "PUT",
      body: JSON.stringify(post),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then((response) => response.json())
      .then((json) => commit("updatePost", json));
  },
  removePost: ({ commit }, postId) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
      method: "DELETE",
    })
      .then((res) => res.json())
      .then(() => {
        commit("removePost", postId);
      });
  },
  getUsers: ({ commit }) => {
    const state = JSON.parse(localStorage.getItem("state"))
    if(state && state.users && state.users.length) {
      commit("setUsers", state.users)
    }else {
      fetch("https://jsonplaceholder.typicode.com/users")
      .then((response) => response.json())
      .then((res) => {
        commit("setUsers", res)
      });
    } 
  },
  chatLogin: ({commit}, user) => {
    commit('chatLogin', user);
  },
  addMessage: ({commit}, message) => {
    commit('addMessage', message);
  }
};
