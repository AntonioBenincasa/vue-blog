import Vue from 'vue';
export default {
    setPosts: (state, posts) => {
        state.posts = [...posts]
    },
    addPost: (state, post) => {
        state.posts = [post, ...state.posts];
    },
    updatePost: (state, updatedPost) => {
        const postIndex = state.posts.findIndex((post) => post.id === updatedPost.id);
        Vue.set(state.posts, postIndex, updatedPost);
    },
    removePost: (state, postId) => {
        const postIndex = state.posts.findIndex(post => post.id === postId);
        state.posts.splice(postIndex, 1);
    },
    setUsers: (state, users) => {
        state.users = users;
    },
    chatLogin: (state, user) => {
        state.chat.user = user;
    },
    addMessage: (state, message) => {
        state.chat.messages.push(message);
    }
}